package com.devcamp.task58_jpa_hibernate.jpahibernate.controller;

import java.util.*;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task58_jpa_hibernate.jpahibernate.model.CVoucher;
import com.devcamp.task58_jpa_hibernate.jpahibernate.repository.IVoucherRepository;

@CrossOrigin
@RestController
public class CVoucherController {
    @Autowired
    IVoucherRepository voucherRepository;

    @GetMapping("/vouchers")
    public ResponseEntity<List<CVoucher>> getVouchers() {
        try {
            List<CVoucher> lstVoucher = new ArrayList<CVoucher>();
            // voucherRepository.findAll().forEach((ele)-> {
            //     lstVoucher.add(ele);
            // });
            voucherRepository.findAll().forEach(lstVoucher::add);
            if (lstVoucher.size() == 0) {
                return new ResponseEntity<List<CVoucher>>(lstVoucher, HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<List<CVoucher>>(lstVoucher, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/vouchers1")
    public ArrayList<CVoucher> getVouchers1() {
        ArrayList<CVoucher> lstVoucher = new ArrayList<CVoucher>();
        try {
            voucherRepository.findAll().forEach(lstVoucher::add);
        } catch (Exception e) {

        }

        return  lstVoucher;
    }
}
