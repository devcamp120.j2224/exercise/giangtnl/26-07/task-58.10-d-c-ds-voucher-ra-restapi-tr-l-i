package com.devcamp.task58_jpa_hibernate.jpahibernate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task58_jpa_hibernate.jpahibernate.model.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {
    
}
